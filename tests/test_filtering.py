import os
import shutil

import pytest

from fun_lib.filtering import filter_csv
from fun_lib.filtering import parse_filters

def __add_row(cols):
    return {
        "partner_id":cols[0],
        "country":cols[1],
        "location":cols[2],
        "date_of_collection":cols[3],
        "study":cols[4],
        "plate_id":cols[5],
        "plate_well_id":cols[6]
    }

@pytest.fixture()
def get_test_csv():
    csv = [__add_row([
        f"part_{i}",
        f"count_{i}",
        f"loc_{i}",
        f"coll_{i}",
        f"study_{i}",
        f"plate_{2 if i > 5 else 1}",
        f"well_{i}",
    ]) for i in range(1,10)]

    return csv

def test_country_filter(get_test_csv):
    result = filter_csv(get_test_csv,{"country":"count_1"})

    assert len(result) == 1
    assert result[0]["country"] == "count_1"

def test_plate_filter(get_test_csv):
    result = filter_csv(get_test_csv,{"plate_id":"plate_1"})

    assert len(result) == 5
    for r in result:
        assert r["plate_id"] == "plate_1"

def test_study_filter(get_test_csv):
    result = filter_csv(get_test_csv,{"study":"study_1"})

    assert len(result) == 1
    assert result[0]["study"] == "study_1"

def test_parse_filters():
    filters = {
        "foo":"one",
        "bar":"two",
        "moo":"three"
    }

    filter_list = [f"{k}:{v}" for k, v in filters.items()]

    assert parse_filters(filter_list) == filters
