import argparse


class ArgumentParserBuilder:

    @staticmethod
    def new_instance(factory=lambda: argparse.ArgumentParser()):
        return ArgumentParserBuilder(factory())


    def __init__(self, parser):
        self.parser = parser
        self.subparsers = self.parser.add_subparsers(help='sub-command help')


    def with_version(self, version):
        self.parser.add_argument(
            '--version', action='version', version=version)
        return self


    def with_count(self):
        filtering_parser = self.subparsers.add_parser("count", help='count number of samples for a given set of filters')
        filtering_parser.add_argument('--input', '-i', dest='input', required=True,
                                  help='Input csv file')
        filtering_parser.add_argument('--filter','-f', dest='filter', required=False, nargs='*')
        filtering_parser.set_defaults(operation="count")
        return self


    def build(self):
        return self.parser
